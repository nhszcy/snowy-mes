import { axios } from '@/utils/request'

export function RejectsResult (parameter) {
    return axios({
        url: '/bigScreen/queryRejects',
        methods: 'get',
        params: parameter
    })
}

export function WorkOrderProResult (parameter) {
    return axios({
        url: '/bigScreen/queryWorkOrderPro',
        methods: 'get',
        params: parameter
    })
}

export function TodayAndConductResult (parameter) {
    return axios({
        url: '/bigScreen/odayAndConduct',
        methods: 'get',
        params: parameter
    })
}

export function WorkOrderAlertResult (parameter) {
    return axios({
        url: '/bigScreen/queryWorkOrderAlert',
        methods: 'get',
        params: parameter
    })
}

export function workStepOrderResult (parameter) {
    return axios({
        url: '/bigScreen/workStepOrder',
        methods: 'get',
        params: parameter
    })
}
