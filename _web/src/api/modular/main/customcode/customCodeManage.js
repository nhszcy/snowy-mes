import { axios } from '@/utils/request'

/**
 * 查询自定义编号规则
 *
 * @author sxy
 * @date 2022-06-28 09:32:04
 */
export function customCodePage (parameter) {
  return axios({
    url: '/customCode/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 自定义编号规则列表
 *
 * @author sxy
 * @date 2022-06-28 09:32:04
 */
export function customCodeList (parameter) {
  return axios({
    url: '/customCode/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加自定义编号规则
 *
 * @author sxy
 * @date 2022-06-28 09:32:04
 */
export function customCodeAdd (parameter) {
  return axios({
    url: '/customCode/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑自定义编号规则
 *
 * @author sxy
 * @date 2022-06-28 09:32:04
 */
export function customCodeEdit (parameter) {
  return axios({
    url: '/customCode/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除自定义编号规则
 *
 * @author sxy
 * @date 2022-06-28 09:32:04
 */
export function customCodeDelete (parameter) {
  return axios({
    url: '/customCode/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出自定义编号规则
 *
 * @author sxy
 * @date 2022-06-28 09:32:04
 */
export function customCodeExport (parameter) {
  return axios({
    url: '/customCode/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

export function customCodeInformationList (parameter) {
  return axios({
    url: '/customCode/InformationList',
    method: 'get',
    params: parameter
  })
}
