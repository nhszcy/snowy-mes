/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.stockbalance.param;

import com.baomidou.mybatisplus.annotation.TableField;
import com.tencentcloudapi.tci.v20190318.models.Face;
import org.springframework.data.redis.connection.Message;
import vip.xiaonuo.core.pojo.base.param.BaseParam;
import lombok.Data;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;
import java.util.*;

/**
* 库存余额参数类
 *
 * @author czw
 * @date 2022-07-28 16:41:37
*/
@Data
public class StockBalanceParam extends BaseParam {

    /**
     * 主键id
     */
    @NotNull(message = "主键id不能为空，请检查id参数", groups = {edit.class, delete.class, detail.class})
    private Long id;

    /**
     * 仓库id
     */
    @NotNull(message = "仓库id不能为空，请检查warHouId参数", groups = {add.class, edit.class})
    private Long warHouId;

    /**
     * 产品id
     */
    @NotBlank(message = "产品id不能为空，请检查proId参数", groups = {add.class, edit.class})
    private Long proId;

    /**
     * 产品ID数组
     */
    private String proIds;



    /**
     * 产品名称
     */
    @NotBlank(message = "产品名称不能为空，请检查proName参数", groups = {add.class, edit.class})
    private String proName;

    /**
     * 仓库名称
     */
    @NotNull(message = "仓库名称不能为空，请检查warHouName参数", groups = {add.class, edit.class})
    private String warHouName;

    /**
     * 库存数量
     */
    @NotNull(message = "库存数量不能为空，请检查stoNum参数", groups = {add.class, edit.class})
    private Long stoNum;


}
