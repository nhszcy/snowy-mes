/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.workorderbill.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.workorderbill.param.WorkOrderBillParam;
import vip.xiaonuo.modular.workorderbill.service.WorkOrderBillService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 工单单据关系表控制器
 *
 * @author wz
 * @date 2022-08-31 14:12:47
 */
@RestController
public class WorkOrderBillController {

    @Resource
    private WorkOrderBillService workOrderBillService;

    /**
     * 查询工单单据关系表
     *
     * @author wz
     * @date 2022-08-31 14:12:47
     */
    @Permission
    @GetMapping("/workOrderBill/page")
    @BusinessLog(title = "工单单据关系表_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(WorkOrderBillParam workOrderBillParam) {
        return new SuccessResponseData(workOrderBillService.page(workOrderBillParam));
    }

    /**
     * 添加工单单据关系表
     *
     * @author wz
     * @date 2022-08-31 14:12:47
     */
    @Permission
    @PostMapping("/workOrderBill/add")
    @BusinessLog(title = "工单单据关系表_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(WorkOrderBillParam.add.class) WorkOrderBillParam workOrderBillParam) {
            workOrderBillService.add(workOrderBillParam);
        return new SuccessResponseData();
    }

    /**
     * 删除工单单据关系表，可批量删除
     *
     * @author wz
     * @date 2022-08-31 14:12:47
     */
    @Permission
    @PostMapping("/workOrderBill/delete")
    @BusinessLog(title = "工单单据关系表_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(WorkOrderBillParam.delete.class) List<WorkOrderBillParam> workOrderBillParamList) {
            workOrderBillService.delete(workOrderBillParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑工单单据关系表
     *
     * @author wz
     * @date 2022-08-31 14:12:47
     */
    @Permission
    @PostMapping("/workOrderBill/edit")
    @BusinessLog(title = "工单单据关系表_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(WorkOrderBillParam.edit.class) WorkOrderBillParam workOrderBillParam) {
            workOrderBillService.edit(workOrderBillParam);
        return new SuccessResponseData();
    }

    /**
     * 查看工单单据关系表
     *
     * @author wz
     * @date 2022-08-31 14:12:47
     */
    @Permission
    @GetMapping("/workOrderBill/detail")
    @BusinessLog(title = "工单单据关系表_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(WorkOrderBillParam.detail.class) WorkOrderBillParam workOrderBillParam) {
        return new SuccessResponseData(workOrderBillService.detail(workOrderBillParam));
    }

    /**
     * 工单单据关系表列表
     *
     * @author wz
     * @date 2022-08-31 14:12:47
     */
    @Permission
    @GetMapping("/workOrderBill/list")
    @BusinessLog(title = "工单单据关系表_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(WorkOrderBillParam workOrderBillParam) {
        return new SuccessResponseData(workOrderBillService.list(workOrderBillParam));
    }

    /**
     * 导出系统用户
     *
     * @author wz
     * @date 2022-08-31 14:12:47
     */
    @Permission
    @GetMapping("/workOrderBill/export")
    @BusinessLog(title = "工单单据关系表_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(WorkOrderBillParam workOrderBillParam) {
        workOrderBillService.export(workOrderBillParam);
    }

}
