/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.workorderbill.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import vip.xiaonuo.core.consts.CommonConstant;
import vip.xiaonuo.core.enums.CommonStatusEnum;
import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.core.util.PoiUtil;
import vip.xiaonuo.modular.workorderbill.entity.WorkOrderBill;
import vip.xiaonuo.modular.workorderbill.enums.WorkOrderBillExceptionEnum;
import vip.xiaonuo.modular.workorderbill.mapper.WorkOrderBillMapper;
import vip.xiaonuo.modular.workorderbill.param.WorkOrderBillParam;
import vip.xiaonuo.modular.workorderbill.service.WorkOrderBillService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.List;

/**
 * 工单单据关系表service接口实现类
 *
 * @author wz
 * @date 2022-08-31 14:12:47
 */
@Service
public class WorkOrderBillServiceImpl extends ServiceImpl<WorkOrderBillMapper, WorkOrderBill> implements WorkOrderBillService {

    @Override
    public PageResult<WorkOrderBill> page(WorkOrderBillParam workOrderBillParam) {
        QueryWrapper<WorkOrderBill> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(workOrderBillParam)) {

        }
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<WorkOrderBill> list(WorkOrderBillParam workOrderBillParam) {
        return this.list();
    }

    @Override
    public void add(WorkOrderBillParam workOrderBillParam) {
        WorkOrderBill workOrderBill = new WorkOrderBill();
        BeanUtil.copyProperties(workOrderBillParam, workOrderBill);
        this.save(workOrderBill);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<WorkOrderBillParam> workOrderBillParamList) {
        workOrderBillParamList.forEach(workOrderBillParam -> {
            this.removeById(workOrderBillParam.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(WorkOrderBillParam workOrderBillParam) {
        WorkOrderBill workOrderBill = this.queryWorkOrderBill(workOrderBillParam);
        BeanUtil.copyProperties(workOrderBillParam, workOrderBill);
        this.updateById(workOrderBill);
    }

    @Override
    public WorkOrderBill detail(WorkOrderBillParam workOrderBillParam) {
        return this.queryWorkOrderBill(workOrderBillParam);
    }

    /**
     * 获取工单单据关系表
     *
     * @author wz
     * @date 2022-08-31 14:12:47
     */
    private WorkOrderBill queryWorkOrderBill(WorkOrderBillParam workOrderBillParam) {
        WorkOrderBill workOrderBill = this.getById(workOrderBillParam.getId());
        if (ObjectUtil.isNull(workOrderBill)) {
            throw new ServiceException(WorkOrderBillExceptionEnum.NOT_EXIST);
        }
        return workOrderBill;
    }

    @Override
    public void export(WorkOrderBillParam workOrderBillParam) {
        List<WorkOrderBill> list = this.list(workOrderBillParam);
        PoiUtil.exportExcelWithStream("SnowyWorkOrderBill.xls", WorkOrderBill.class, list);
    }

}
