package vip.xiaonuo.modular.cusperson.Result;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import vip.xiaonuo.modular.cusperson.entity.CusPerson;
@Data
public class CusPersonResult extends CusPerson {
    /**
     * 客户资料名称
     */
    @Excel(name = "客户资料名称")
    private String cusInforName;
}
