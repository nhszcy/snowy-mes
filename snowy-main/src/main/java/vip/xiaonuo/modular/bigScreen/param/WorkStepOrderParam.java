package vip.xiaonuo.modular.bigScreen.param;

import lombok.Data;

@Data
public class WorkStepOrderParam {
    //是否查询全部
    private Boolean queryNotStart;
}
