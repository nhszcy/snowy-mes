/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.suppsort.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.core.pojo.node.AntdBaseTreeNode;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.modular.suppsort.entity.SuppSort;
import vip.xiaonuo.modular.suppsort.param.SuppSortParam;

import java.util.List;

/**
 * 供应商分类service接口
 *
 * @author zjk
 * @date 2022-08-04 13:41:54
 */
public interface SuppSortService extends IService<SuppSort> {

    /**
     * 查询供应商分类
     *
     * @author zjk
     * @date 2022-08-04 13:41:54
     */
    PageResult<SuppSort> page(SuppSortParam suppSortParam);

    /**
     * 供应商分类列表
     *
     * @author zjk
     * @date 2022-08-04 13:41:54
     */
    List<SuppSort> list(SuppSortParam suppSortParam);

    /**
     * 添加供应商分类
     *
     * @author zjk
     * @date 2022-08-04 13:41:54
     */
    void add(SuppSortParam suppSortParam);

    /**
     * 删除供应商分类
     *
     * @author zjk
     * @date 2022-08-04 13:41:54
     */
    void delete(List<SuppSortParam> suppSortParamList);

    /**
     * 编辑供应商分类
     *
     * @author zjk
     * @date 2022-08-04 13:41:54
     */
    void edit(SuppSortParam suppSortParam);

    /**
     * 查看供应商分类
     *
     * @author zjk
     * @date 2022-08-04 13:41:54
     */
     SuppSort detail(SuppSortParam suppSortParam);

    /**
     * 导出供应商分类
     *
     * @author zjk
     * @date 2022-08-04 13:41:54
     */
     void export(SuppSortParam suppSortParam);


    /**
     * 获取组织树
     * @author ZJK
     * @date 2022-08-03 09:25:48
     */
    List<AntdBaseTreeNode> tree(SuppSortParam suppSortParam);


    /**
     * 根据节点id获取所有子节点id集合
     * @author ZJK
     * @date 2022-08-03 09:25:48
     */
    List<Long> getChildIdListById(Long id);
}