package vip.xiaonuo.modular.cusinfor.Result;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import vip.xiaonuo.modular.cusinfor.entity.CusInfor;
@Data
public class CusInforResult extends CusInfor {
    /**
     * 父类型名称
     */
    @Excel(name = "父类型名称")
    private String cusSortName;
}
