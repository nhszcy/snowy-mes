/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.invIn.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.invIn.param.InvInParam;
import vip.xiaonuo.modular.invIn.service.InvInService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 入库单控制器
 *
 * @author wz
 * @date 2022-06-06 15:49:08
 */
@RestController
public class InvInController {

    @Resource
    private InvInService invInService;

    /**
     * 查询入库单
     *
     * @author wz
     * @date 2022-06-06 15:49:08
     */
    @Permission
    @GetMapping("/invIn/page")
    @BusinessLog(title = "入库单_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(InvInParam invInParam) {
        return new SuccessResponseData(invInService.page(invInParam));
    }

    /**
     * 添加入库单
     *
     * @author wz
     * @date 2022-06-06 15:49:08
     */
    @Permission
    @PostMapping("/invIn/add")
    @BusinessLog(title = "入库单_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(InvInParam.add.class) InvInParam invInParam) {
            invInService.add(invInParam);
        return new SuccessResponseData();
    }

    /**
     * 删除入库单，可批量删除
     *
     * @author wz
     * @date 2022-06-06 15:49:08
     */
    @Permission
    @PostMapping("/invIn/delete")
    @BusinessLog(title = "入库单_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(InvInParam.delete.class) List<InvInParam> invInParamList) {
            invInService.delete(invInParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑入库单
     *
     * @author wz
     * @date 2022-06-06 15:49:08
     */
    @Permission
    @PostMapping("/invIn/edit")
    @BusinessLog(title = "入库单_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(InvInParam.edit.class) InvInParam invInParam) {
            invInService.edit(invInParam);
        return new SuccessResponseData();
    }

    /**
     * 查看入库单
     *
     * @author wz
     * @date 2022-06-06 15:49:08
     */
    @Permission
    @GetMapping("/invIn/detail")
    @BusinessLog(title = "入库单_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(InvInParam.detail.class) InvInParam invInParam) {
        return new SuccessResponseData(invInService.detail(invInParam));
    }

    /**
     * 入库单列表
     *
     * @author wz
     * @date 2022-06-06 15:49:08
     */
    @Permission
    @GetMapping("/invIn/list")
    @BusinessLog(title = "入库单_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(InvInParam invInParam) {
        return new SuccessResponseData(invInService.list(invInParam));
    }
    /**
     * 导出系统用户
     *
     * @author wz
     * @date 2022-06-06 15:49:08
     */
    @Permission
    @GetMapping("/invIn/export")
    @BusinessLog(title = "入库单_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(InvInParam invInParam) {
        invInService.export(invInParam);
    }

}
